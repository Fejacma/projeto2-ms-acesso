package br.com.itau.acesso.models;

import javax.persistence.*;

@Entity
public class Acesso {


    @Column
    private Long portaID;

    @Column
    private Long clienteID;

    public Acesso() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getPortaID() {
        return portaID;
    }

    public void setPortaID(Long portaID) {
        this.portaID = portaID;
    }

    public Long getClienteID() {
        return clienteID;
    }

    public void setClienteID(Long clienteID) {
        this.clienteID = clienteID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
