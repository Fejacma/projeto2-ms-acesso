package br.com.itau.acesso.repositories;

import br.com.itau.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {

    Optional<Acesso> findByPortaIDAndClienteID(Long portaID, Long clienteID);
}
