package br.com.itau.acesso.controllers;

import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso cadastrar(@Valid @RequestBody Acesso acesso){
        acesso = acessoService.cadastrar(acesso);
        return acesso;
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletar(@RequestParam(name = "cliente_id") Long clienteID,
                        @RequestParam(name = "porta_id") Long portaID){
        Acesso acesso = new Acesso();
        acesso.setClienteID(clienteID);
        acesso.setPortaID(portaID);
        acessoService.deletar(acesso);
    }


    @GetMapping
    public Acesso buscar(@RequestParam(name = "cliente_id") Long clienteID,
                         @RequestParam(name = "porta_id") Long portaID){
        Acesso acesso = new Acesso();
        acesso.setClienteID(clienteID);
        acesso.setPortaID(portaID);
        return acessoService.buscar(acesso);
    }


}

