package br.com.itau.acesso.services.Kafka.DTO;

import java.util.Date;

public class LogAcesso {

    private int cliente_Id;
    private int porta_Id;
    private Date dtSolicitacao;
    private boolean permissao;

    public LogAcesso(){}

    public LogAcesso(int cliente_Id, int porta_Id, Date dtSolicitacao, boolean permissao) {
        this.cliente_Id = cliente_Id;
        this.porta_Id = porta_Id;
        this.dtSolicitacao = dtSolicitacao;
        this.permissao = permissao;
    }

    public int getCliente_Id() {
        return cliente_Id;
    }

    public void setCliente_Id(int cliente_Id) {
        this.cliente_Id = cliente_Id;
    }

    public int getPorta_Id() {
        return porta_Id;
    }

    public void setPorta_Id(int porta_Id) {
        this.porta_Id = porta_Id;
    }

    public Date getDtSolicitacao() {
        return dtSolicitacao;
    }

    public void setDtSolicitacao(Date dtSolicitacao) {
        this.dtSolicitacao = dtSolicitacao;
    }

    public boolean isPermissao() {
        return permissao;
    }

    public void setPermissao(boolean permissao) {
        this.permissao = permissao;
    }

}
