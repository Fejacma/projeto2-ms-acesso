package br.com.itau.acesso.services.Kafka;

import br.com.itau.acesso.services.Kafka.DTO.LogAcesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class LogAcessoProducer {

    @Autowired
    private KafkaTemplate<String, LogAcesso> producer;

    public void enviarAoKafka(LogAcesso logAcesso) {
        // manda para o tópico e qualquer um pode pegar a msg (somente 1)
        producer.send("spec3-fernando-jacyntho-1", logAcesso);

    }

}
