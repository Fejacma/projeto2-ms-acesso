package br.com.itau.acesso.services;

import br.com.itau.acesso.clients.ClienteClient;
import br.com.itau.acesso.clients.PortaClient;
import br.com.itau.acesso.models.Acesso;
import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;
import br.com.itau.acesso.repositories.AcessoRepository;
import br.com.itau.acesso.services.Kafka.DTO.LogAcesso;
import br.com.itau.acesso.services.Kafka.LogAcessoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private LogAcessoProducer logAcessoProducer;


    public Acesso cadastrar(Acesso acesso) {
        Porta porta = portaClient.getPortaById(acesso.getPortaID());
        Cliente cliente = clienteClient.consultarClientePorId(acesso.getClienteID());
        acesso.setPortaID(porta.getId());
        acesso.setClienteID(cliente.getId());

        return acessoRepository.save(acesso);
    }


    public void deletar(Acesso acesso){

        Optional<Acesso> acessoDelete = acessoRepository.findByPortaIDAndClienteID(acesso.getPortaID(),
                acesso.getClienteID());

        if(!acessoDelete.isPresent()){
            throw new RuntimeException();
        }

        acessoRepository.delete(acessoDelete.get());
    }


    public Acesso buscar(Acesso acesso){
        Optional<Acesso> acessoObjeto = acessoRepository.findByPortaIDAndClienteID(acesso.getPortaID(),
                acesso.getClienteID());


        int cliente_Id = 0;
        int porta_Id = 0;
        if(!acessoObjeto.isPresent()){

            LogAcesso logAcesso = new LogAcesso(cliente_Id, porta_Id,new Date(),false);
            logAcessoProducer.enviarAoKafka(logAcesso);

            throw new RuntimeException();
        }

        LogAcesso logAcesso = new LogAcesso(cliente_Id,porta_Id,new Date(),true);
        logAcessoProducer.enviarAoKafka(logAcesso);

        return acessoObjeto.get();

    }


}
