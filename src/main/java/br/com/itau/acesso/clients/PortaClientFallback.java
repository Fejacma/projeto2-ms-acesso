package br.com.itau.acesso.clients;

import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;

public class PortaClientFallback implements PortaClient {

    @Override
    public Porta getPortaById(Long id) {
        throw new PortaOfflineException();
//        return null;
    }
}
