package br.com.itau.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de Porta se encontra offline")
public class PortaOfflineException extends RuntimeException {
}

