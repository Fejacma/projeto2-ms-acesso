package br.com.itau.acesso.clients;

import br.com.itau.acesso.models.Cliente;
import br.com.itau.acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta getPortaById(@PathVariable Long id);

}
