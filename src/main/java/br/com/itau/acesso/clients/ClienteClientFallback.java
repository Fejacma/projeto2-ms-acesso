package br.com.itau.acesso.clients;

import br.com.itau.acesso.models.Cliente;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente consultarClientePorId(Long id) {
        throw new ClienteOfflineException();
//        return null;
    }
}
